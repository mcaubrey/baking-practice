<?php

App::uses('Security', 'Utility');

class User extends AppModel {
    
    public $displayField = 'firstname';
    
    public $hasMany = array(
        'Product'
    );
    
    public $validate = array (
        'firstname' => array(
                'rule' => 'notBlank',
                'message' => 'Please fill in a first name.'
            ),
        'lastname' => array(
                'rule' => 'notBlank',
                'message' => 'Please fill in a last name.'
            ),
        'email' => array(
                'emailRule-1' => array (
                        'rule' => 'notBlank',
                        'message' => 'Please fill out an email address.'
                    ),
                'emailRule-2' => array (
                        'rule' => 'email',
                        'message' => 'Please fill out a valid email address.'
                    )
            ),
        'password' => array(
                'rule' => array ('between', 4, 10),
                'message' => 'Password must be between 4 and 10 characters long.'
            )
        );
        
        public function beforeSave($options = array()) {
            
            $this->data['User']['password'] = Security::hash($this->data['User']['password'], 'sha1', true);
            
            return true;
        }
}

?>