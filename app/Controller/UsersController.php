<?php

class UsersController extends AppController {
    
    Public function index() {
        
        $users = $this->User->find('all');

        $this->set('users', $users);
        
    }
    
    Public function add() {
        
        if($this->request->is('post')) {
            
            $this->User->save($this->request->data);
            $this->redirect('/users');
            
        }
        
    }
    
    Public function login() {
        
        if( $this->request->is('post') ){
            
            // 1. Find method
            
            $user = $this->User->find('first', array(
                'conditions' => array(
                    'email' => $this->request->data('User.email'),
                    'password' => Security::hash($this->request->data('User.password'), 'sha1', true)
                )
            )); 
            
            
            // 2. Magical find
            
            //$user = $this->User->findByEmailAndPassword($this->request->data('User.email'), $this->request->data('User.password'));
            
            if ($user) {
                $this->Session->write('User', $user);
                $this->redirect(array(
                    'controller' => 'home',
                    'action' => 'index'
                ));
            }
            
            $this->Session->setFlash('Email and password combination were incorrect.');
            
        }
    }
}

?>